from scipy import signal
import matplotlib as plt
import numpy as np


def notch_filter(x, freq, Q=50.0, sampling_freq=2000):
    b, a = signal.iirnotch(w0=freq, Q=Q, fs=sampling_freq)
    x_filtered = signal.filtfilt(b, a, x, axis=0)
    return x_filtered
