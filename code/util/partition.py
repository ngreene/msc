import numpy as np


def stratified_split(data, labels, ratio):
    partition1_indices = []
    partition2_indices = []
    labels_np = np.array(labels)
    unique = np.unique(labels_np)
    for label in unique:
        ii = np.where(labels_np == label)[0]
        split = int(ratio * ii.size)
        partition1_indices.append(ii[0:split])
        partition2_indices.append(ii[split:])

    partition1_indices = np.concatenate(partition1_indices, axis=0)
    partition2_indices = np.concatenate(partition2_indices, axis=0)

    partition1_classes = [data[i] for i in partition1_indices]
    partition1_labels = [labels[i] for i in partition1_indices]
    partition2_classes = [data[i] for i in partition2_indices]
    partition2_labels = [labels[i] for i in partition2_indices]

    return (partition1_classes, partition1_labels,
            partition2_classes, partition2_labels)

