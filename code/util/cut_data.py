import os
import re
from typing import Optional

import numpy as np
import pandas as pd

from get_min import get_min_length
from feature_scaling import standard_scaler
np.set_printoptions(formatter={'float': '{: 0.4f}'.format})


def run(source_dir='../data/notch_filtered/', dest_dir='../data/uncompressed/cut/'):
    arrays = []
    names = []
    for f in os.listdir(source_dir):
        if re.match('^[0-9][0-9]-0[1-7]-0[1-6][.]npy$', f):
            arrays.append(np.load(source_dir + f))
            names.append(f)

    min_length = arrays[0].shape[0]
    for arr in arrays:
        length = arr.shape[0]
        if length < min_length: min_length = length

    print(min_length)
    for i, arr in enumerate(arrays):
        df = pd.DataFrame(arr)
        df = df[-min_length:]
        df = standard_scaler(df) # normalise
        arr_new = df.to_numpy()
        if not os.path.isdir(dest_dir): os.makedirs(dest_dir)
        print('Saving', dest_dir + names[i])
        np.save(dest_dir + names[i], arr_new)

run()