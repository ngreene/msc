def get_max_length(fnames: []) -> int:
    max = 0
    for fname in fnames:
        with open(fname, 'r') as f:
            row_num = sum(1 for row in f)
            if row_num > max: max = row_num
    return max
