# Contains preprocessing functions for data normalisation.
import pandas as pd
from sklearn.preprocessing import MinMaxScaler


def standard_scaler(data: pd.DataFrame) -> pd.DataFrame:
    '''Normalises the data column wise as (x - x_mean) / x_stan_dev.
    '''
    assert not data.empty, 'Dataframe is empty!'
    return (data - data.mean()) / data.std()

def min_max_scaler(data: pd.DataFrame) -> pd.DataFrame:
    assert not data.empty, 'Dataframe is empty!'

    return (data - data.mean()) / data.max()

def max_absolute_scaler(data: pd.DataFrame) -> pd.DataFrame:
    return # not implemented

def unit_scaler(data: pd.DataFrame) -> pd.DataFrame:
    return # not implemented

