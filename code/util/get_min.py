import numpy as np

def get_min_length(fnames: []) -> int:
    min = np.inf
    for fname in fnames:
        with open(fname, 'r') as f:
            row_num = sum(1 for row in f)
            if row_num < min: min = row_num
    return min
