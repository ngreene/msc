#!usr/bin/env python3

import os
import re
from typing import Optional

import numpy as np
import pandas as pd

from get_max import get_max_length
from feature_scaling import standard_scaler


def run(source_dir='../data/raw_csv/', dest_dir='../data/uncompressed/', pad='zero'):
    def csv_to_numpy(f_path: str, length=-1) -> Optional[np.ndarray]:
        assert length > 0, 'length must be greater than 0'
        df = pd.read_csv(f_path)
        df = df.drop(df.columns[[0, -1]], axis=1)
        df = standard_scaler(df) # normalise
        if pad == 'zero':
            df = df.reindex(range(max_length), fill_value=0)
        elif pad == 'repeat':
            initial_row_size = df.shape[0]
            frames = []
            current_size = 0
            while(True):
                frames.append(df)
                current_size += initial_row_size
                if current_size >= length:
                    df = pd.concat(frames)[0:max_length]
                    break

        return df.to_numpy()

    csv_files = []
    for participant in os.listdir(source_dir):
        if re.match('^[0-9][0-9]$', participant):
            for trial_csv in os.listdir(source_dir + participant):
                if re.match('^[0-9][0-9]-0[2-7]-0[1-3][.]csv$', trial_csv):
                    csv_files.append(source_dir + participant + '/' + trial_csv)

    max_length = get_max_length(csv_files)
    dest_dir += (pad + '-pad/')
    for trial_csv in csv_files:
        trial_np = csv_to_numpy(trial_csv, max_length)
        save_name = trial_csv.split('/')[-1].split('.')[0] + '.npy'
        if not os.path.isdir(dest_dir): os.makedirs(dest_dir)
        print('Saving', dest_dir + save_name)
        np.save(dest_dir + save_name, trial_np)


def csv_to_numpy(f_path: str) -> Optional[np.ndarray]:
    df = pd.read_csv(f_path)
    df = df.drop(df.columns[[0, -1]], axis=1)

    return df.to_numpy()
