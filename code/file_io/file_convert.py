import pandas as pd
import numpy as np

from typing import Optional


def csv_to_numpy(f_path: str) -> Optional[np.ndarray]:
    df = pd.read_csv(f_path)
    return df.to_numpy()
