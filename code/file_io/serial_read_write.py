import pickle
import gzip


def serial_write(wr, dest_path: str):
    file_extension = dest_path.split('.')[-1]
    assert file_extension == 'pkl', '{0} has wrong file type'.format(dest_path)
    with gzip.open(dest_path, 'wb') as f:
        pickle.dump(wr, f)


def serial_read(source_path: str):
    file_extension = source_path.split('.')[-1]
    assert file_extension == 'pkl', '{0} has wrong file type'.format(source_path)
    with gzip.open(source_path, 'rb') as f:
        ret = pickle.load(f)
    return ret
