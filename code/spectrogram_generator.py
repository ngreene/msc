# Python standard libraries
import os
import re
from PIL import Image

# External libraries
import matplotlib.image as mpimg
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import mlab as mlab
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
import cv2

# My files
from preprocessing import get_clips_with_events


def random_middle_index(min, max, size, rng):
    lowest_possible = min + size // 2
    highest_possible = max - (size + 1) // 2
    if lowest_possible >= highest_possible:
        middle_index = highest_possible
    else:
        middle_index = rng.integers(low=lowest_possible, high=highest_possible)
    return middle_index


def resize_data(filenames):
    rng = np.random.default_rng(42)
    min_size = np.load(filenames[0]).shape[0]
    for ID in filenames[1:]:
        size = np.load(ID).shape[0]
        if size < min_size: min_size = size
    window_size = min_size
    source_dir = '/'.join(filenames[0].split('/')[:-1]) + '/'
    clips_with_events = get_clips_with_events(source_dir)
    X = np.zeros((len(filenames), window_size, 5))
    IDs = []

    for i, fname in enumerate(filenames):
        arr = np.load(fname)
        ID = fname.split('.')[0].split('/')[-1]
        IDs.append(ID)
        events = clips_with_events.get(ID[-5:], None)
        rows = arr.shape[0]
        if events is not None:
            middle_index = int(np.median(events))
        else:
            middle_index = random_middle_index(int((2 / 3) * rows), rows,
                                               window_size, rng)
        start_index = middle_index - window_size // 2
        if start_index < 0:
            start_index = 0
        end_index = start_index + window_size
        if end_index > arr.shape[0]:
            end_index = arr.shape[0]
            start_index = end_index - window_size

        try:
            X[i] = arr[start_index:end_index]
        except ValueError as e:
            print()
            print('!', rows, start_index, end_index, end_index - start_index)
            print()
            raise e

    return X, IDs


def show_spectrogram(x, sampling_rate, title=None, rnge=None, vmin=None, vmax=None):

    fig, ax = plt.subplots()
    spec, freqs, t, im = ax.specgram(x, Fs=sampling_rate, scale='dB',
                                     vmin=vmin, vmax=vmax)
    ax.set_ylim(rnge)

    plt.suptitle(title)
    plt.xlabel('Time (s)')
    plt.ylabel('Frequency (Hz)')
    plt.tight_layout()
    # plt.axis('off')
    cbar = plt.colorbar(im, ax=ax)
    cbar.set_label('Amplitude (dB)')
    cbar.minorticks_on()

    plt.show()


def save_spectrogram(x, sampling_rate, savename, rnge, vmin, vmax):
    fig, ax = plt.subplots()
    binary_repr_vec = np.vectorize(np.binary_repr)
    bin_range = binary_repr_vec(np.arange(8), width=3)
    for i in range(bin_range.size):
        channel_noise_bool = [bool(int(x)) for x in bin_range[i]]
        channels = []
        for noise in channel_noise_bool:
            if noise:
                channel = x + np.random.normal(0, 0.01, x.shape)
            else:
                channel = x
            spec, freqs, t, im = ax.specgram(channel, Fs=sampling_rate, scale='dB',
                                             vmin=vmin, vmax=vmax)
            ax.set_ylim(rnge)
            plt.axis('off')
            # fig.savefig(savename, bbox_inches='tight', pad_inches=0)
            # print('Saved', savename)
            # plt.close(fig)
            plt.subplots_adjust(0, 0, 1, 1, 0, 0)
            canvas = FigureCanvas(fig)
            canvas.draw()
            image_from_plot = np.frombuffer(fig.canvas.tostring_rgb(), dtype=np.uint8)
            image_from_plot = image_from_plot.reshape(fig.canvas.get_width_height()[::-1] + (3,))
            plt.close(fig)
            # im = Image.fromarray(image_from_plot)
            # im.save(savename)
            img = cv2.resize(image_from_plot, (299, 299))
            img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            img = np.reshape(img, np.shape(img) + (1,))
            channels.append(img)
        img_3_channel = np.concatenate(channels, axis=2)
        # plt.imshow(image_from_plot)
        # plt.show()
        # plt.imshow(img, cmap='gray')
        # plt.show()
        # plt.imshow(img_3_channel)
        # plt.show()
        cv2.imwrite(savename + '_{0:02d}.png'.format(i), img_3_channel)
        # plt.imshow(image_from_plot)
        # plt.suptitle('Before')
        # plt.show()
        # plt.imshow(img, cmap='gray')
        # plt.suptitle('After')
        # plt.show()


def min_max_from_spectral_content(x, sampling_rate):
    spec, freqs, t, = mlab.specgram(x, Fs=sampling_rate)
    vmin = 10 * np.log10(spec.min())
    vmax = 10 * np.log10(spec.max())
    return vmin, vmax


in_dir = 'data/notch_filtered/'
fnames = []
for f in os.listdir(in_dir):
    if re.match('.*npy$', f):
        fnames.append(in_dir + f)

data, IDs = resize_data(fnames)
sampling_rate = 2000

for channel in range(4):
    channel = 3

    vmins = []
    vmaxs = []
    for i in range(data.shape[0]):
        vmin, vmax = min_max_from_spectral_content(data[i, :, channel], sampling_rate=sampling_rate)
        vmins.append(vmin)
        vmaxs.append(vmax)

    overall_vmin = np.percentile(vmins, 25)
    overall_vmax = np.percentile(vmaxs, 75)

    ranges = {
        0: [0, 500],
        1: [0, 500],
        2: [0, 110],
        3: [0, 25]
    }
    freq_range = ranges[channel]

    paths = {
        0: 'data/spec_img/zyg/',
        1: 'data/spec_img/cor/',
        2: 'data/spec_img/ecg/',
        3: 'data/spec_img/res/'
    }

    out_path = paths[channel]
    if not os.path.exists(out_path): os.makedirs(out_path)
    for i, ID in enumerate(IDs):
        title = out_path + ID
        x = data[i, :, channel]
        show_spectrogram(x, sampling_rate, title=ID, rnge=freq_range,
                         vmin=overall_vmin, vmax=overall_vmax)
        # save_spectrogram(x, sampling_rate, savename=title, rnge=freq_range,
        #                  vmin=overall_vmin, vmax=overall_vmax)
        print(i+1, 'Saved', title)
