# Python standard libraries
import os
import re

# External libraries
import numpy as np
from scipy import signal
from scipy.fft import fftshift
from matplotlib import mlab
import matplotlib.pyplot as plt


def spectrogram_all_channels(x, sampling_rate, title=None):
    yaxis_names = [
        'Z. fEMG',
        'C. fEMG',
        'ECG',
        'Resp.'
    ]
    fig, axs = plt.subplots(nrows=4, sharex=True)
    for i, ax in enumerate(axs):
        spec, freqs, t, im = ax.specgram(x[:,i], Fs=sampling_rate, scale='dB', vmax=0)
        ax.set_ylim([0, 100])
        ax.set_ylabel(yaxis_names[i], rotation=0, ha='right')
        ax.get_yaxis().set_ticks([])

    plt.suptitle(title)
    plt.xlabel('Time (s)')
    plt.tight_layout()
    cbar = plt.colorbar(im, ax=axs)
    cbar.set_label('Amplitude (dB)')
    cbar.minorticks_on()


def spectrogram(x, sampling_rate, title=None):
    # fig, axs = plt.subplots(nrows=4, sharex=True)
    # for i, ax in enumerate(axs):
    #     spec, freqs, t, im = ax.specgram(x[:,i], Fs=sampling_rate, scale='dB', vmax=0)
    #     ax.set_ylim([0, 100])
    #     ax.set_ylabel(yaxis_names[i], rotation=0, ha='right')
    #     ax.get_yaxis().set_ticks([])

    fig, ax = plt.subplots()
    spec, freqs, t, im = ax.specgram(x, Fs=sampling_rate, scale='dB')

    plt.suptitle(title)
    plt.xlabel('Time (s)')
    plt.ylabel('Frequency (Hz)')
    plt.tight_layout()
    cbar = plt.colorbar(im, ax=ax)
    cbar.set_label('Amplitude (dB)')
    cbar.minorticks_on()

sampling_rate = 2000
in_path = 'data/raw_npy/'
in_path_filtered = 'data/notch_filtered/'
out_path = 'spectrogram_img/'

for f in os.listdir(in_path):
    if re.match('^[0-9][0-9]-[0-9][0-9]-[0-9][0-9][.]npy$', f):
        title = f.split('.')[0]
        data = np.load(in_path + f)
        # spectrogram(data, sampling_rate, title=title)
        plt.show()

        data = np.load(in_path_filtered + f)
        # spectrogram_all_channels(data, sampling_rate, title=title + '_filtered')
        spectrogram(data[:,0], sampling_rate, title=title)
        # plt.show()
        # if not os.path.exists(out_path):
        #     os.makedirs(out_path)

        # savename = out_path + f + '.png'
        # plt.savefig(savename, dpi=400)