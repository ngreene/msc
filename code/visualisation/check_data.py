import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.fft import fft, fftfreq, fftshift

# Frequencies above 100Hz seem to look flat...so we will not plot them
upperFreq = 100

# Sampling rate (in seconds I presume)
s = 0.0005


def time_and_freq(x, title):
    # Get the number of samples and number of channels
    nSamples, nChannels = np.shape(x)
    # nChannels -= 1

    yaxis_names = [
        'Z. fEMG',
        'C. fEMG',
        'ECG',
        'Resp.'
    ]

    fh = plt.figure()

    # Time axis
    t = np.arange(nSamples) * s

    for i in range(nChannels):
        ph = fh.add_subplot(nChannels, 2, i * 2 + 1)

        # Plot time signal
        ph.plot(t, x[:, i])

        ph.get_yaxis().set_ticks([])
        ph.set_ylabel(yaxis_names[i], rotation=0, ha='right')
        if (i == (nChannels - 1)):
            ph.set_xlabel('Time (s)')
        else:
            ph.get_xaxis().set_ticks([])

        # Take an FFT and shift it so that zero freq is in the middle
        X = fftshift(fft(x[:, i]))
        # Compute freq (in Hz) for corresponding samples of X
        freq = fftshift(fftfreq(nSamples, d=s))

        # Remove negative frequencies (they are just the mirror of positive ones, since the
        # original signal is all real (i.e. not complex)
        X = X[freq >= 0]
        freq = freq[freq >= 0]

        # Remove high frequencies from the graph - they look mostly flat
        if upperFreq > 0:
            X = X[freq <= upperFreq]
            freq = freq[freq <= upperFreq]

        # Plot the magnitude of the FFT
        ph = fh.add_subplot(nChannels, 2, 2 * (i + 1))
        ph.get_yaxis().set_ticks([])

        plt.plot(freq, np.abs(X), color='tab:orange')


        if (i == (nChannels - 1)):
            ph.set_xlabel('Freq (Hz)')
        else:
            ph.get_xaxis().set_ticks([])

        # This is how we get the phase...but we will ignore it for now
        # ph = fh.add_subplot(len(channels), 3, i * 3 + 3)
        # plt.plot(freq, np.angle(C))
        plt.suptitle(title)

    return fh


def freq_log_scale(x, title):
    # Get the number of samples and number of channels
    nSamples, nChannels = np.shape(x)
    # nChannels -= 1

    yaxis_names = [
        'Z. fEMG',
        'C. fEMG',
        'ECG',
        'Resp.'
    ]

    fh = plt.figure()

    # Time axis
    t = np.arange(nSamples) * s

    for i in range(nChannels):


        # Take an FFT and shift it so that zero freq is in the middle
        X = fftshift(fft(x[:, i]))
        # Compute freq (in Hz) for corresponding samples of X
        freq = fftshift(fftfreq(nSamples, d=s))

        # Remove negative frequencies (they are just the mirror of positive ones, since the
        # original signal is all real (i.e. not complex)
        X = X[freq >= 0]
        freq = freq[freq >= 0]

        # Plot the magnitude of the FFT
        ph = fh.add_subplot(nChannels, 1, i+1)
        ph.get_yaxis().set_ticks([])
        ph.set_ylabel(yaxis_names[i], rotation=0, ha='right')
        ph.set_xlim([10**-1, 500])
        ph.set_xscale('log')

        plt.plot(freq, np.abs(X), color='tab:orange')

        ph.get_yaxis().set_ticks([])
        if (i == (nChannels - 1)):
            ph.set_xlabel('Freq (Hz)')
        else:
            ph.get_xaxis().set_ticks([])

        plt.suptitle(title)

    return fh


def plot_time_and_freq_domain(x, title):
    time_and_freq(x, title)
    plt.show()


def save_plot_time_and_freq_domain(x, title):
    fig = time_and_freq(x, title)
    out_path = 'img/notch_before_after/'
    if not os.path.exists(out_path): os.makedirs(out_path)
    savename = out_path + title + '.png'
    plt.savefig(savename, dpi=400)
    plt.close(fig)
    print('Saved to', savename)


def plot_freq_log_scale(x, title):
    freq_log_scale(x, title)
    plt.show()


def save_plot_freq_log_scale(x, title):
    fig = freq_log_scale(x, title)
    out_path = 'img/freq_log_scale/'
    if not os.path.exists(out_path): os.makedirs(out_path)
    savename = out_path + title + '.png'
    plt.savefig(savename, dpi=400)
    plt.close(fig)
    print('Saved to', savename)
