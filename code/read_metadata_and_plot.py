import os
import pickle

import numpy as np
from sklearn.metrics import classification_report, confusion_matrix
import matplotlib.pyplot as plt
import seaborn as sns

import file_io

in_path = 'tmp/'
best_models = []
for modality in os.listdir(in_path):
    best_model = {}
    best_model['modality'] = modality
    best_mean_accuracy = 0.0
    for model in os.listdir(in_path + modality):
        test_accuracies = []
        test_losses = []
        y_pred = []
        y_true = []
        train_accuracy_histories = []
        train_loss_histories = []
        # val_accuracy_histories = []
        # val_loss_histories = []
        best_epochs = []
        hidden_layers = None
        folds = os.listdir(in_path + modality + '/' + model)
        if len(folds) < 10: continue
        for fold in folds:
            metadata_path = in_path + modality + '/' + model + '/' + fold + '/'
            data = file_io.serial_read(metadata_path + 'metadata.pkl')
            test_accuracies.append(data['final_test_accuracy'])
            test_losses.append(data['final_test_loss'])
            y_pred.append(data['y_pred'])
            y_true.append(data['y_true'])
            train_accuracy_histories.append(data['training_accuracy_hist'])
            train_loss_histories.append(data['training_loss_hist'])
            # val_accuracy_histories.append(data['validation_accuracy_hist'])
            # val_loss_histories.append(data['validation_loss_hist'])
            best_epochs.append(int(data['best_epoch']))
            hidden_layers = data['hidden_layers']
            ls_factor = data['l2_factor']
        mean_accuracy = np.mean(test_accuracies)
        if mean_accuracy > best_mean_accuracy:
            best_mean_accuracy = mean_accuracy
            best_model['test_accuracies'] = test_accuracies
            best_model['test_losses'] = test_losses
            best_model['y_pred'] = np.concatenate(y_pred)
            best_model['y_true'] = np.concatenate(y_true)
            best_model['train_accuracy_histories'] = train_accuracy_histories
            best_model['train_loss_histories'] = train_loss_histories
            # best_model['val_accuracy_histories'] = val_accuracy_histories
            # best_model['val_loss_histories'] = val_loss_histories
            best_model['best_epochs'] = best_epochs
            best_model['hidden_layers'] = hidden_layers
            best_model['l2_factor'] = ls_factor

    best_models.append(best_model)

labels_names = ['Calm', 'Happy', 'Sad', 'Anger', 'Fear', 'Disgust']
full_modality_name = {
    'zyg': 'Z. fEMG',
    'cor': 'C. fEMG',
    'ecg': 'ECG',
    'res': 'Resp.'
}

for best_model in best_models:
    modality = full_modality_name[best_model['modality']]
    print('Modality:', modality)
    print(classification_report(y_true=best_model['y_true'], y_pred=best_model['y_pred'],
                                target_names=labels_names))
    cm = confusion_matrix(y_true=best_model['y_true'], y_pred=best_model['y_pred'])
    # accuracies = confusion_mat.diagonal()/confusion_mat.sum(axis=1)
    ax = plt.subplot()
    sns.heatmap(cm, annot=True, fmt='g', ax=ax, cmap='Reds')

    # labels, title and ticks
    ax.set_xlabel('Predicted labels')
    ax.set_ylabel('True labels')
    ax.set_title(modality)
    ax.xaxis.set_ticklabels(labels_names)
    ax.yaxis.set_ticklabels(labels_names)
    plt.show()

    fold = 0
    plt.figure()
    plt.plot(best_model['train_accuracy_histories'][fold], label='training_accuracy', zorder=5)
    # plt.plot(best_model['val_accuracy_histories'][fold], label='validation_accuracy', zorder=0)
    plt.vlines(best_model['best_epochs'][fold]-1, 0, 1, colors='red', zorder=10)
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    plt.ylim([0.10, 0.5])
    # plt.xlim(0, 2*best_model['best_epochs'][fold])
    plt.legend(loc='lower right')
    plt.suptitle(modality)
    plt.show()

    plt.figure()
    plt.plot(best_model['train_loss_histories'][fold], label='training_loss')
    # plt.plot(best_model['val_loss_histories'][fold], label='validation_loss')
    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    plt.ylim([0, 10])
    plt.legend(loc='lower right')
    plt.suptitle(modality)
    plt.show()

print()






