import os
import re

import numpy as np
import cv2
from tensorflow import keras
from tensorflow.keras.regularizers import l2
import matplotlib.pyplot as plt


os.environ['CUDA_DEVICE_ORDER'] = 'PCI_BUS_ID'
os.environ['CUDA_VISIBLE_DEVICES'] = '1'

data_path = 'data/temp/'
X_train_fname = 'X_train.npy'
y_train_fname = 'y_train.npy'
X_val_fname = 'X_val.npy'
y_val_fname = 'y_val.npy'
X_test_fname = 'X_test.npy'
y_test_fname = 'y_test.npy'
if not os.path.exists(data_path):
    in_path = 'data/img/'
    file_names = []
    labels = []

    num_classes = 0
    for subdir in os.listdir(in_path):
        if re.match('^[0-9][0-9]$', subdir):
            full_path = in_path + subdir + '/'
            for f in os.listdir(full_path):
                if re.match('.*[.]JPEG$', f):
                       file_names.append(full_path + f)
                       labels.append(num_classes)
            num_classes += 1

    N = len(file_names)
    X = np.zeros((N, 299, 299, 3))
    y = keras.utils.to_categorical(np.array(labels), num_classes=num_classes)
    for i, fname in enumerate(file_names):
        X[i] = cv2.imread(fname)

    indices = np.arange(N)
    np.random.shuffle(indices)

    X = X[indices]
    y = y[indices]

    split1 = int(0.7 * N)
    split2 = int(0.85 * N)

    X_train = X[:split1]
    y_train = y[:split1]
    X_val = X[split1:split2]
    y_val = y[split1:split2]
    X_test = X[split2:]
    y_test = y[split2:]

    os.makedirs(data_path)
    np.save(data_path + X_train_fname, X_train)
    np.save(data_path + y_train_fname, y_train)
    np.save(data_path + X_val_fname, X_val)
    np.save(data_path + y_val_fname, y_val)
    np.save(data_path + X_test_fname, X_test)
    np.save(data_path + y_test_fname, y_test)
else:
    intermediate_path = data_path + 'intermediate/'
    if not os.path.exists(intermediate_path):
        X_train = np.load(data_path + X_train_fname)
        X_val = np.load(data_path + X_val_fname)
        X_test = np.load(data_path + X_test_fname)

        base_model = keras.applications.InceptionResNetV2(include_top=False, weights='imagenet')
        inputs = keras.Input(shape=(None, None, 3))
        x = base_model(inputs)
        outputs = keras.layers.GlobalAvgPool2D()(x)
        model = keras.models.Model(inputs, outputs)

        X_train_output = model.predict(X_train)
        X_val_output = model.predict(X_val)
        X_test_output = model.predict(X_test)

        os.makedirs(intermediate_path)
        np.save(intermediate_path + X_train_fname, X_train_output)
        np.save(intermediate_path + X_val_fname, X_val_output)
        np.save(intermediate_path + X_test_fname, X_test_output)
    else:
        X_train = np.load(intermediate_path + X_train_fname)
        X_val = np.load(intermediate_path + X_val_fname)
        X_test = np.load(intermediate_path + X_test_fname)
        y_train = np.load(data_path + y_train_fname)
        y_val = np.load(data_path + y_val_fname)
        y_test = np.load(data_path + y_test_fname)

        # base_model.trainable = False
        # inputs = keras.Input(shape=(None, None, 3))
        # x = base_model(inputs)

        inputs = keras.Input(shape=(X_train.shape[-1]))
        x = keras.layers.Dense(1024, activation='relu', kernel_regularizer=l2(), bias_regularizer=l2())(inputs)
        x = keras.layers.Dense(1024, activation='relu', kernel_regularizer=l2(), bias_regularizer=l2())(x)
        x = keras.layers.Dense(1024, activation='relu', kernel_regularizer=l2(), bias_regularizer=l2())(x)
        x = keras.layers.Dense(512, activation='relu', kernel_regularizer=l2(), bias_regularizer=l2())(x)
        outputs = keras.layers.Dense(10, activation='softmax')(x)
        model = keras.models.Model(inputs, outputs)
        model.compile(optimizer=keras.optimizers.Adam(),
                      loss='categorical_crossentropy',
                      metrics='accuracy')
        early_stopping_callback = keras.callbacks.EarlyStopping(
            monitor='val_loss',  # monitored value
            min_delta=0,  # acceptable difference in monitored value
            patience=300  # number of epochs between checks
        )
        train_info = model.fit(x=X_train, y=y_train,
                               batch_size=500,
                               validation_data=(X_val, y_val),
                               epochs=5000,
                               callbacks=[early_stopping_callback])
        final_train_loss, final_train_acc = model.evaluate(X_train, y_train)
        final_val_loss, final_val_acc = model.evaluate(X_val, y_val)
        final_test_loss, final_test_acc = model.evaluate(X_test, y_test)

        print('\nFinal train accuracy (tf): %.2f' % final_train_acc)
        print('Final val accuracy (tf): %.2f' % final_val_acc)
        print('Final test accuracy (tf): %.2f' % final_test_acc)

        plt.figure()
        plt.plot(train_info.history['accuracy'], label='training_accuracy')
        plt.plot(train_info.history['val_accuracy'], label='validation_accuracy')
        plt.xlabel('Epoch')
        plt.ylabel('Accuracy')
        # plt.ylim([0.16, 1])
        plt.legend(loc='lower right')

        plt.show()

        plt.figure()
        plt.plot(train_info.history['loss'], label='training_loss')
        plt.plot(train_info.history['val_loss'], label='validation_loss')
        plt.xlabel('Epoch')
        plt.ylabel('Loss')
        plt.ylim([0, 6])
        plt.legend(loc='lower right')

        plt.show()





