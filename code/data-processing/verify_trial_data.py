#!/u sr/bin/env python3

# data_extract_main.py - This is the file you run to extract and verify
# the trial data from the original acqknowledge (acq) files.

def get_true_running_time(clip):
    with open('running-times.txt', 'r') as f:
        for line in f:
            filename = line.split()[0]
            running_time = line.split()[1]
            if filename == clip: return float(running_time)

# Calculates the running
# trial_mat: the trial matrix
# T: the sampling interval (seconds / sample)
def calc_trial_running_times(trial_mat, T):
    running_times = []
    for i in range(trial_mat.shape[0]):
        running_times.append((trial_mat[i, 1] - trial_mat[i, 0]) * T)
        # minutes, seconds = divmod(running_time, 60)
        # print(trial_mat[i], '{0}:{1:02.0f}'.format(int(minutes), seconds))

    return running_times