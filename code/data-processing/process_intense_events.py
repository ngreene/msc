#!/usr/bin/env python3

# add_intense_events.py -

__author__ = 'Nicholas Greene'
__copyright__ = 'Copyright 2021 Nicholas Greene'
__license__ = 'MIT'
__version__ = '1.0'

# Python standard libraries
import os
import re
from bisect import bisect_left

# External libraries
import pandas as pd
import numpy as np

def find_nearest(arr, x):
    idx = np.searchsorted(arr, x, 'left')
    if idx == 0: return 0, arr[0]
    if idx == len(arr): return len(arr)-1, arr[-1]

    before = arr[idx-1]
    after = arr[idx]
    if abs(after - x) < abs(before - x):
        return idx, after
    return idx-1, before


def append_intense_events(df, id, filename):
    times = df.iloc[:, 0].values
    events = np.zeros(df.shape[0], dtype=int)

    with open('../participant-responses/' + id + '_exp.csv') as f:
        for line in f:
            spl = line.strip('\n').split(',')
            curr_filename = spl[0]
            reg = '^' + filename.rsplit('.', 1)[0] + '\.mp4$'
            if re.match(reg, id + '-' + curr_filename):
                spl.pop(0) # remove filename
                if spl:
                    print(filename)
                    it = iter(spl)
                    for x in it:
                        ev = x
                        time = float(next(it))
                        idx, nearest = find_nearest(times, time)
                        events[idx] = int(ev)
                        print('\t{0} {1:.4f} {2:.4f} {3}'.format(idx, time, nearest, events[idx]))
                    print()

    event_df = pd.DataFrame(events.T)

    return pd.concat([df, event_df], axis=1)

# data_dir = 'trial-data/'
# out_dir = data_dir + 'processed/'
# if not os.path.exists(out_dir):
#     os.makedirs(out_dir)
#
# for participant_id in os.listdir(data_dir):
#     if not re.match('^[0-9][0-9]$', participant_id): continue
#     full_path = data_dir + participant_id + '/'
#     for filename in os.listdir(full_path):
#         if re.match('^[0-9][0-9]-[0-9][0-9]-[0-9][0-9].csv$', filename):
#             with open(full_path + filename) as f:
#                 curr_data = pd.read_csv(f)
#                 if curr_data.shape[1] == 5:
#                     complete_data = append_intense_events(curr_data,
#                                                           participant_id,
#                                                           filename)
#
#                     participant_dir = out_dir + participant_id + '/'
#                     if not os.path.exists(participant_dir):
#                         os.makedirs(participant_dir)
#
#                     complete_data.to_csv(participant_dir + filename,
#                                          index=False,
#                                          header=False)
#                     # print(complete_data)

