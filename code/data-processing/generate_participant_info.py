#!/usr/bin/env python3

# generate_participant_info.py -

import os
import re

import numpy as np

np.set_printoptions(formatter={'float': '{: 0.4f}'.format})


def generate_participant_info(trial_dir, id):
    participant_dir = trial_dir + id + '/'
    info_file = participant_dir + id + '-info.csv'
    if not os.path.exists(info_file):
        with open(info_file, 'w') as f:
            f.write('Filename,Chills,Tears,Startle\n')

        with open('../participant-responses/' + id + '.csv') as read_f:
            read_f.readline()
            for line in read_f:
                movie = line.split(',')[0]
                trial_csv = (participant_dir + id + '-'
                             + movie.split('.')[0] + '.csv')
                trial_mat = np.genfromtxt(trial_csv, delimiter=',')
                unique, counts = np.unique(trial_mat[:,-1], return_counts=True)
                unique = unique.astype(int)

                wr = movie
                index = 1
                wr += ','
                if 1 in unique:
                    wr += str(counts[index])
                    index += 1
                else:
                    wr += '0'
                wr += ','
                if 2 in unique:
                    wr += str(counts[index])
                    index += 1
                else:
                    wr += '0'
                wr += ','
                if 3 in unique:
                    wr += str(counts[index])
                    index += 1
                else:
                    wr += '0'
                wr += '\n'

                with open(info_file, 'a') as write_f:
                    write_f.write(wr)


# trial_dir = '../trial-data/'
# for id in os.listdir(trial_dir):
#     if not re.match('^[0-9][0-9]', id): continue
#     print(id)
#     generate_participant_info(trial_dir, id)
#     print()
