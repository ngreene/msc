#!usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

np.set_printoptions(formatter={'float': '{: 0.4f}'.format})

def plot_acq(data, name, indices=None):
    global colours
    n = len(data.channels)
    if colours.size == 0: colours = np.random.rand(n, 3)
    fig, axs = plt.subplots(n)
    fig.suptitle(name, fontsize='x-large')
    w, h = 12, 4
    # xmin, xmax = 0, 20
    # ymin, ymax = -1, 1.5
    for i in range(n):
        curr = data.channels[i]
        # curr_plt = plt.figure(i+1, figsize=(w, h), dpi=80)
        curr_data = curr.data if indices is None else curr.data[indices]
        curr_ti = curr.time_index if indices is None else curr.time_index[indices]
        axs[i].plot(curr_ti,
                    curr_data,
                    label='{} ({})'.format(curr.name, curr.units),
                    c=colours[i])
        axs[i].set_title(curr.name)
        # plt.xlim([xmin, xmax])
        # plt.ylim([ymin-i, ymax+(i*0.5)])

    plt.draw()


def print_acq_info(data):
    for chan in data.channels:
        print('\t- Name:', chan.name)
        print('\t- Time:', chan.time_index)
        print('\t- Data:', chan.data)
        print('\t- Time_len:', len(chan.time_index))
        print('\t- Data_len:', len(chan.data))
        print()


def acq_to_np_matrix(data):
    names = []
    mat = np.empty([len(data.channels[0].data), len(data.channels)])
    # mat[:, 0] = data.channels[0].time_index
    for i in range(len(data.channels)):
        names.append(data.channels[i].name)
        mat[:, i] = data.channels[i].data

    return mat, names


def acq_to_dataframe(data):
    df = pd.DataFrame(data.channels)
    # for i in range(len(data.channels)):
    #     df.loc[i] = data.channels[i]
    # df = pd.DataFrame(columns=data.channels[0].time_index)
    # for i in range(len(data.channels)-1):
    #     df.loc[i] = data.channels[i].data
    return df


def get_clip_list(participant_id):
    response_file = participant_id + '.csv'
    clip_list = []
    with open('../participant-responses/' + response_file, 'r') as f:
        lines = f.readlines()[1:]
        for line in lines: clip_list.append(line.split(',')[0])
    return clip_list

