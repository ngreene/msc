#!/usr/bin/env python3

# running-times.py - 

import os
import subprocess
import re

def get_running_time(filename):
    result = subprocess.run(['ffprobe', '-v', 'error', '-show_entries',
                            'format=duration', '-of',
                            'default=noprint_wrappers=1:nokey=1', filename],
                            stdout=subprocess.PIPE,
                            stderr=subprocess.STDOUT)
    return float(result.stdout)

clip_dir = '/Volumes/MY_COOL_USB/induction-clips/'
clips = []
for filename in os.listdir(clip_dir):
    if re.match('^0[1-7]-0[1-6].mp4$', filename): clips.append(filename)

for clip in clips:
    runtime = get_running_time(clip_dir + clip)
    print('{0} {1:7.3f}'.format(clip, runtime))
