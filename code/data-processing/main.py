#!/usr/bin/env python3

# acq_data_extract.py -

__author__ = 'Nicholas Greene'
__copyright__ = 'Copyright 2021 Nicholas Greene'
__license__ = 'MIT'
__version__ = '1.0'

# Python standard library
import os
import re

# External libraries
import sys

import bioread
import numpy as np

# My files
from reformat_data import get_trial_matrix, save_trial_data
from verify_trial_data import get_true_running_time, calc_trial_running_times
from generate_participant_info import generate_participant_info
from util import get_clip_list, acq_to_np_matrix, print_acq_info

np.set_printoptions(formatter={'float': '{: 0.4f}'.format})
colours = np.array([])

in_path = '../phys-data/'

acq_files = []
for filename in os.listdir(in_path):
    if re.match('^[0-9][0-9].acq$', filename): acq_files.append(filename)
acq_files.sort()

participant_ids = []
for filename in acq_files:
    participant_ids.append(os.path.splitext(filename)[0])

out_path = '../data/raw_csv/'
if not os.path.exists(out_path):
    os.makedirs(out_path)

remove = []
for id in participant_ids:
    usr_dir = out_path + str(id) + '/'
    # If participant data has already been processed, remove participant
    # from lists
    if os.path.exists(usr_dir):
        remove.append(id)
        continue
    os.makedirs(usr_dir)

for id in remove:
    participant_ids.remove(id)
    acq_files.remove(id + '.acq')

if not participant_ids:
    print('\nNothing new to process')
    sys.exit(0)

matrices = []
col_names = []
participant_clips = []
sampling_rate = 0
for filename in acq_files:
    delimiter = '*' * 80
    print(delimiter)
    data = bioread.read_file(in_path + filename)

    participant_id = os.path.splitext(filename)[0]
    clip_list = get_clip_list(participant_id)
    participant_clips.append(clip_list)

    if sampling_rate == 0: sampling_rate = data.samples_per_second
    print(filename)
    print_acq_info(data)
    mat, col_names = acq_to_np_matrix(data)
    matrices.append(mat)

col_names = col_names[0:-1]

trial_matrices = []
calculated_running_times = []
T = 1.0 / sampling_rate
for i in range(len(matrices)):
    trial_column = matrices[i][:, -1]
    matrices[i] = matrices[i][:, 0:-1]
    trial_matrix = get_trial_matrix(trial_column)
    trial_matrices.append(trial_matrix)
    calculated_running_times.append(calc_trial_running_times(trial_matrix, T))

print()

for i in range(len(participant_ids)):
    print()
    print(participant_ids[i], '\t{0:9}  {1:<7}  {2:<8}  {3}'.format('Filename',
                                                                    'Time',
                                                                    'Est-time',
                                                                    'Error'))
    for j in range(len(participant_clips[i])):
        running_time = get_true_running_time(participant_clips[i][j])
        diff = abs(running_time - calculated_running_times[i][j])
        print('\t{0}  {1:<7.3f}  {2:<7.3f}   {3:<7.3f}'.format(participant_clips[i][j],
                                                               running_time,
                                                               calculated_running_times[i][j],
                                                               diff))

for i in range(len(participant_ids)):
    save_trial_data(out_path,
                    participant_ids[i],
                    participant_clips[i],
                    matrices[i],
                    trial_matrices[i],
                    T)
    generate_participant_info(out_path, participant_ids[i])
