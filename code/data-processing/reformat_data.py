#!/usr/bin/env python3

# extract_trial_data.py -

# Python standard libraries
import os

# External libraries
import numpy as np
import pandas as pd

# My libraries
from process_intense_events import append_intense_events


# Returns a trial index matrix with dimensions rows * 2. The values in each row
# represent the beginning and end indices of the corresponding trial.
def get_trial_matrix(col):
    # All indices where a trial is occurring
    trial_indices = np.argwhere(col > 2.5).flatten()

    # Do a basic search for value jumps in the trial indices. This separates
    # the indices for each trial from the others.
    value = trial_indices[0]
    result = [trial_indices[0]]
    for i in range(1, len(trial_indices)):
        if trial_indices[i] == value + 1:
            value += 1
        else:
            result.append(trial_indices[i - 1])
            result.append(trial_indices[i])
            value = trial_indices[i]
    result.append(trial_indices[-1])

    return np.array(result).reshape(len(result) // 2, 2)[1:, :]


def save_trial_data(saved_dir, id, clip_list, mat, trial_matrix, T):
    trials = []
    for i in range(trial_matrix.shape[0]):
        start_index = trial_matrix[i, 0]
        end_index = trial_matrix[i, 1]
        trials.append(mat[start_index:end_index + 1, :])

    usr_dir = saved_dir + id + '/'
    for i in range(len(trials)):
        clip = os.path.splitext(clip_list[i])[0]
        time_col = np.arange(trials[i].shape[0]) * T
        trial = np.zeros((trials[i].shape[0], trials[i].shape[1] + 1))
        trial[:, 1:] = trials[i]
        trial[:, 0] = time_col

        data = pd.DataFrame(trial)
        filename = str(id) + '-' + str(clip) + '.csv'
        complete_data = append_intense_events(data, id, filename)
        complete_data.to_csv(usr_dir + filename, index=False, header=False)
