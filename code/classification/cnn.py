#!/data/ngreene/anaconda3/envs/msc/bin/python3

import random

# External libraries
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.regularizers import l2


def build_model(input_shape, n_classes, res_filters=None, optn_batch_norm=False,
                optn_dropout=False, learning_rate_type='adam', random_seed=None):
    rnd = random.Random(random_seed)
    tf.random.set_seed(random_seed)
    if not res_filters:
        res_filters = []
        for _ in range(rnd.randrange(2, 6)):
            res_filters.append(rnd.choice([16, 24, 36, 64, 96, 128, 256]))
        res_filters = sorted(res_filters)

    def res_block(input, filters, k_size, s, reg, pad):
        x = keras.layers.Conv1D(filters=filters,
                                kernel_size=k_size,
                                strides=s,
                                kernel_regularizer=reg,
                                bias_regularizer=reg,
                                padding=pad)(input)
        if optn_batch_norm:
            x = keras.layers.BatchNormalization()(x)

        x = keras.layers.Activation(activation='relu')(x)

        if optn_dropout:
            x = keras.layers.Dropout(0.5)(x)

        x = keras.layers.Conv1D(filters=filters,
                                kernel_size=k_size,
                                strides=1,
                                kernel_regularizer=reg,
                                bias_regularizer=reg,
                                padding=pad)(x)
        if optn_batch_norm:
            x = keras.layers.BatchNormalization()(x)
        shortcut = keras.layers.Conv1D(filters=filters,
                                       kernel_size=k_size,
                                       strides=s,
                                       padding=pad)(input)
        combined = keras.layers.add([x, shortcut])

        output = keras.layers.Activation(activation='relu')(combined)

        if optn_dropout:
            x = keras.layers.Dropout(0.5)(x)

        return output

    layer_regularizer = l2(0.01)
    kernel_size = 5
    padding = 'same'

    hyper_parameters = {}
    hyper_parameters['seed_value'] = random_seed
    hyper_parameters['res_filters'] = res_filters
    hyper_parameters['kernel_size'] = kernel_size
    hyper_parameters['batch_norm'] = optn_batch_norm
    hyper_parameters['dropout'] = optn_batch_norm

    print('\nCNN parameters:')
    print('\tseed', random_seed)
    print('\tres_filters', res_filters)
    print('\tkernel_size', kernel_size)
    print('\tbatch_norm', optn_batch_norm)
    print('\tdropout', optn_dropout)
    print()

    input_layer = keras.layers.Input(shape=input_shape[1:], batch_size=input_shape[0])
    layer = keras.layers.Conv1D(filters=res_filters[0],
                                kernel_size=5,
                                strides=4,
                                kernel_regularizer=layer_regularizer,
                                bias_regularizer=layer_regularizer,
                                padding=padding)(input_layer)
    prev = keras.layers.MaxPool1D(pool_size=4, padding=padding)(layer)
    last_filter = res_filters[0]
    for i, filter in enumerate(res_filters):
        # Down-sample each time the filter size changes
        if filter != last_filter:
            stride = 4
            last_filter = filter
        else:
            stride = 1
        curr = res_block(
            input=prev,
            filters=filter,
            k_size=kernel_size,
            s=stride,
            reg=layer_regularizer,
            pad=padding)
        prev = curr

    gap_layer = keras.layers.GlobalAvgPool1D()(prev)
    output_layer = keras.layers.Dense(n_classes, activation='softmax')(gap_layer)

    if learning_rate_type == 'adam':
        optimiser = keras.optimizers.Adam()
    elif learning_rate_type == 'cyclical' or learning_rate_type == 'step' or 'sgd':
        momentum = 0.9
        optimiser = keras.optimizers.SGD(momentum=momentum)
        hyper_parameters['SGD_momentum'] = momentum

    model = keras.models.Model(inputs=input_layer, outputs=output_layer)
    model.compile(loss='categorical_crossentropy',
                  optimizer=optimiser,
                  metrics='accuracy')

    return model, hyper_parameters
