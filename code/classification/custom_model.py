#!usr/bin/env python3

import tensorflow as tf

class CustomModel(tf.keras.Model):
    #
    # def __init__(self, batch_size, **kwargs):
    #     self.batch_size = batch_size
    #     self.accumulated_gradients = tf.GradientTape()
    #     super(CustomModel, self).__init__(kwargs)

    def train_step(self, data):
        x, y = data

        with tf.GradientTape() as tape:
            y_pred = self(x, training=True) # Forward pass
            loss = self.compiled_loss(y, y_pred, regularization_losses=self.losses)

        # Compute gradients
        trainable_vars = self.trainable_variables
        gradients = tape.gradient(loss, trainable_vars)

        # Update weights
        self.optimizer.apply_gradients(zip(gradients, trainable_vars))
        # Update metrics (includes loss)
        self.compiled_metrics.update_state(y, y_pred)
        # Return a dict mapping metric names to current value
        return {m.name: m.result() for m in self.metrics}

    # def _accumulate_gradients(self):
    #     self.