#!/data/ngreene/anaconda3/envs/msc/bin/python3

# Python standard libraries
import os

# External libraries
import tensorflow as tf
import matplotlib.pyplot as plt

# My packages
import file_io


def save(model, model_type, train_acc, test_acc, folds=-1, metadata={}):
    def cross_validation_save():
        nonlocal model_type, folds
        version = 1
        curr_fold = 1
        save_dir = 'saved/cross-val/{0}_{1:02d}/'.format(model_type, version)
        cross_val_name = '{0}{1}_{2:02d}.{3}'.format(save_dir, model_type, version, curr_fold)
        while os.path.isfile(cross_val_name + '.h5'):
            if curr_fold < folds:
                curr_fold += 1
            else:
                curr_fold = 1
                version += 1
            save_dir = 'saved/cross-val/{0}_{1:02d}/'.format(model_type, version)
            cross_val_name = '{0}{1}_{2:02d}.{3}'.format(save_dir, model_type, version, curr_fold)
        if not os.path.exists(save_dir): os.makedirs(save_dir)
        return cross_val_name

    def train_val_test_save():
        nonlocal model_type
        version = 1
        save_dir = 'saved/train-val-test/{0}_{1:02d}/'.format(model_type, version)
        train_val_test_name = '{0}{1}_{2:02d}'.format(save_dir, model_type, version)
        while os.path.isfile(train_val_test_name + '.h5'):
            version += 1
            save_dir = 'saved/train-val-test/{0}_{1:02d}/'.format(model_type, version)
            train_val_test_name = '{0}{1}_{2:02d}'.format(save_dir, model_type, version)
        if not os.path.exists(save_dir): os.makedirs(save_dir)
        return train_val_test_name

    if folds > 0:
        model_name = cross_validation_save()
    else:
        model_name = train_val_test_save()

    model_save_name = model_name + '.h5'
    accuracy_plot_name = model_name + '_accuracy.png'
    loss_plot_name = model_name + '_loss.png'
    model_arch_name = model_name + '_model.png'
    metadata_name = model_name + '.pkl'

    model.save(model_save_name)
    print('\nSaved model to', model_save_name)
    #
    # with open(final_acc_save_name, 'w') as f:
    #     f.write(f'train_acc {train_acc}\ntest_acc {test_acc}\n')
    # print('Saved accuracies to', final_acc_save_name)
    #
    #
    # with gzip.open(history_save_name, 'wb') as f:
    #     pickle.dump(history, f)
    # print('Saved history to', history_save_name)

    file_io.serial_write(metadata, metadata_name)
    print('Saved metadata to', metadata_name)

    plt.figure()
    plt.plot(metadata['training_accuracy'], label='training_accuracy')
    try:
        plt.plot(metadata['validation_accuracy'], label='validation_accuracy')
    except KeyError:
        pass
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    plt.ylim([0.16, 1])
    plt.legend(loc='lower right')

    plt.savefig(accuracy_plot_name)
    print('Saved figure to', accuracy_plot_name)

    plt.figure()
    plt.plot(metadata['training_loss'], label='training_loss')
    try:
        plt.plot(metadata['validation_loss'], label='validation_loss')
    except KeyError:
        pass
    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    plt.ylim([0, 10])
    plt.legend(loc='lower right')

    plt.savefig(loss_plot_name)
    print('Saved figure to', loss_plot_name)

    # tf.keras.utils.plot_model(model,
    #                           to_file=model_arch_name,
    #                           show_shapes=True,
    #                           show_layer_names=True)
    # print('Saved arch diagram to', model_arch_name)

    print()
