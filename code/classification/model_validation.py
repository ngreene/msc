#!/data/ngreene/anaconda3/envs/msc/bin/python3

# Python standard libraries
import os
import pickle
import re
import random

# External libraries
import numpy as np

np.set_printoptions(formatter={'float': '{: 0.6f}'.format})


class Validator:

    def __init__(self, data_dir, file_type='npy', seed=42, row_to_trial=None):
        self.rng = random.Random(seed)
        self.data_dir = data_dir
        self.file_type = file_type
        self.row_to_trial = row_to_trial
        cl_grouped = self._get_classes() # classes grouped into sublists
        self.classes = self._flatten(cl_grouped)
        self.labels = [[label] * len(cl) for label, cl in enumerate(cl_grouped)]
        self.labels = self._flatten(self.labels)
        self.n_classes = len(cl_grouped)
        self.data_length = len(self.classes)

    def k_fold_stratified(self, k: int):
        '''Partitions the data and labels into k stratified folds.
        '''
        data_folds = [[] for _ in range(k)]
        labels = [[] for _ in range(k)]

        for i, cl in enumerate(self.classes):
            data_folds[i % k] += [cl]
            labels[i % k] += [self.labels[i]]

        return data_folds, labels

    def train_validation_test_split(self, ratio=0.7):
        '''Partitions the data and labels into train, validation, and test sets.
        The ratio is the proportion of data allocated to the training set, and
        the remaining data is split evenly between the validation and test sets.
        The data is stratified between sets.
        '''
        train_indices = []
        val_indices = []
        test_indices = []
        labels_np = np.array(self.labels)
        unique = np.unique(labels_np)
        for label in unique:
            ii = np.where(labels_np == label)[0]
            split1 = int(ratio * ii.size)
            split2 = int((ratio + (1.0-ratio)/2.0) * ii.size)
            train_indices.append(ii[0:split1])
            val_indices.append(ii[split1:split2])
            test_indices.append(ii[split2:])

        train_indices = np.concatenate(train_indices, axis=0)
        val_indices = np.concatenate(val_indices, axis=0)
        test_indices = np.concatenate(test_indices, axis=0)

        train_classes = [self.classes[i] for i in train_indices]
        train_labels = [self.labels[i] for i in train_indices]
        val_classes = [self.classes[i] for i in val_indices]
        val_labels = [self.labels[i] for i in val_indices]
        test_classes = [self.classes[i] for i in test_indices]
        test_labels = [self.labels[i] for i in test_indices]

        return (train_classes, train_labels,
                val_classes, val_labels,
                test_classes, test_labels)

    # **************************************************************************
    # Begin internal helper functions
    # **************************************************************************

    # Returns a list of data file names organised by class.
    # Each item of the returned list is a sublist of the file names for a
    # single class.
    def _get_classes(self):
        class1 = []
        class2 = []
        class3 = []
        class4 = []
        class5 = []
        class6 = []
        class7 = []

        if not self.row_to_trial:
            for trial in os.listdir(self.data_dir):
                if re.match('^[0-9][0-9]-01-0[1-6][.]{0}$'.format(self.file_type), trial):
                    class1.append(self.data_dir + trial)
                elif re.match('^[0-9][0-9]-02-0[1-3][.]{0}$'.format(self.file_type), trial):
                    class2.append(self.data_dir + trial)
                elif re.match('^[0-9][0-9]-03-0[1-3][.]{0}$'.format(self.file_type), trial):
                    class3.append(self.data_dir + trial)
                elif re.match('^[0-9][0-9]-04-0[1-3][.]{0}$'.format(self.file_type), trial):
                    class4.append(self.data_dir + trial)
                elif re.match('^[0-9][0-9]-05-0[1-3][.]{0}$'.format(self.file_type), trial):
                    class5.append(self.data_dir + trial)
                elif re.match('^[0-9][0-9]-06-0[1-3][.]{0}$'.format(self.file_type), trial):
                    class6.append(self.data_dir + trial)
                elif re.match('^[0-9][0-9]-07-0[1-3][.]{0}$'.format(self.file_type), trial):
                    class7.append(self.data_dir + trial)
        else:
            for row, trial in self.row_to_trial.items():
                if re.match('.*-01-[0-6]$', trial):
                    class1.append(row)
                elif re.match('.*-02-0[1-3]$', trial):
                    class2.append(row)
                elif re.match('.*-03-0[1-3]$', trial):
                    class3.append(row)
                elif re.match('.*-04-0[1-3]$', trial):
                    class4.append(row)
                elif re.match('.*-05-0[1-3]$', trial):
                    class5.append(row)
                elif re.match('.*-06-0[1-3]$', trial):
                    class6.append(row)
                elif re.match('.*-07-0[1-3]$', trial):
                    class7.append(row)

        self.rng.shuffle(class1)
        self.rng.shuffle(class2)
        self.rng.shuffle(class3)
        self.rng.shuffle(class4)
        self.rng.shuffle(class5)
        self.rng.shuffle(class6)
        self.rng.shuffle(class7)

        class1 = class1[:len(class2)] # Fix class imbalance
        # return [class1, class2, class3, class4, class5, class6, class7]
        return [class2, class3, class4, class5, class6, class7]

    # Flattens a list of sublists into a single list.
    def _flatten(self, x):
        return [item for sublist in x for item in sublist]

    # **************************************************************************
    # End internal helper functions
    # **************************************************************************
