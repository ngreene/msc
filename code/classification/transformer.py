# Python standard libraries
import random

# External libraries
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.regularizers import l2

# My files
from scheduling import CustomSchedule


def transformer_encoder(inputs, head_size, num_heads, ff_dim, dropout=0):
    # Normalisation and attention
    x = layers.LayerNormalization(epsilon=1e-6)(inputs)
    x = layers.MultiHeadAttention(
        key_dim=head_size, num_heads=num_heads, dropout=dropout
    )(x, x)
    x = layers.Dropout(dropout)(x)
    res = x + inputs

    # Feed forward
    x = layers.LayerNormalization(epsilon=1e-6)(res)
    x = layers.Conv1D(filters=ff_dim, kernel_size=1, activation='relu')(x)
    x = layers.Dropout(dropout)(x)
    x = layers.Conv1D(filters=inputs.shape[-1], kernel_size=1)(x)

    return x + res


def down_sampling_block(inputs, filters, dropout, reg=None):
    # First filter block with large kernel
    x = layers.LayerNormalization(epsilon=1e-6)(inputs)
    x = layers.Conv1D(filters=filters[0],
                            kernel_size=200,
                            strides=2,
                            kernel_regularizer=reg,
                            bias_regularizer=reg,
                            padding='same',
                            activation='relu')(x)
    x = layers.MaxPool1D(pool_size=2, padding='same')(x)

    # Remaining filter blocks
    for filter in filters[1:]:
        x = layers.LayerNormalization(epsilon=1e-6)(x)
        x = layers.Conv1D(filters=filter,
                                kernel_size=5,
                                strides=2,
                                kernel_regularizer=reg,
                                bias_regularizer=reg,
                                padding='same',
                                activation='relu')(x)
        x = layers.MaxPool1D(pool_size=2, padding='same')(x)
        x = layers.Dropout(dropout)(x)
    return x


def build_model(
    input_shape,
    n_classes,
    head_size=None,
    num_heads=None,
    ff_dim=None,
    num_transformer_blocks=None,
    mlp_units=None,
    dropout=None,
    mlp_dropout=None,
    downsampling_units=None,
    learning_rate_type='adam',
    random_seed=None
):
    hyper_parameters = {}
    rnd = random.Random(random_seed)
    hyper_parameters['seed_value'] = random_seed
    if not head_size:
        head_size = rnd.choice([256, 512, 1024])
        hyper_parameters['head_size'] = head_size
    if not num_heads:
        num_heads = rnd.randrange(3, 9)
        hyper_parameters['num_heads'] = num_heads
    if not ff_dim:
        ff_dim = rnd.randrange(3, 9)
        hyper_parameters['ff_dim'] = ff_dim
    if not num_transformer_blocks:
        num_transformer_blocks = rnd.randrange(3, 9)
        hyper_parameters['num_transformer_blocks'] = num_transformer_blocks
    if not mlp_units:
        mlp_units = []
        for _ in range(rnd.randrange(1, 5)):
            mlp_units.append(rnd.choice([32, 64, 96, 128]))
        mlp_units = sorted(mlp_units)
        hyper_parameters['mlp_units'] = mlp_units
    if not mlp_dropout:
        mlp_dropout = rnd.uniform(0.0, 0.75)
        hyper_parameters['mlp_dropout'] = mlp_dropout
    if not dropout:
        dropout = rnd.uniform(0.0, 0.75)
        hyper_parameters['dropout'] = dropout
    if not downsampling_units:
        downsampling_units = [16, 16, 16, 16]
        hyper_parameters['downsampling_units'] = downsampling_units

    print('\nTransformer hyper-parameters:')
    print('\tseed', random_seed)
    print('\thead_size', head_size)
    print('\tnum_heads', num_heads)
    print('\tff_dim', ff_dim)
    print('\tnum_transformer_blocks', num_transformer_blocks)
    print('\tmlp_units', mlp_units)
    print('\tmlp_dropout', mlp_dropout)
    print('\tdropout', dropout)
    print('\tdownsampling_units', downsampling_units)
    print()

    inputs = keras.Input(shape=input_shape[1:], batch_size=input_shape[0])
    x = down_sampling_block(inputs=inputs, filters=downsampling_units,
                            reg=None, dropout=dropout)

    for _ in range(num_transformer_blocks):
        x = transformer_encoder(x, head_size, num_heads, ff_dim, dropout)

    x = layers.GlobalAveragePooling1D(data_format='channels_last')(x)
    for dim in mlp_units:
        x = layers.Dense(dim, activation='relu')(x)
        x = layers.Dropout(mlp_dropout)(x)
    outputs = layers.Dense(n_classes, activation='softmax')(x)

    #model = CustomModel(inputs, outputs)
    model = keras.models.Model(inputs=inputs, outputs=outputs)
    learning_rate = CustomSchedule(head_size)

    if learning_rate_type == 'adam':
        # adam_learning_rate = rnd.uniform(0.0005, 0.004)
        # adam_epsilon = rnd.uniform(1e-8, 4e-7)
        # optimizer = keras.optimizers.Adam(learning_rate=adam_learning_rate,
        #                                   epsilon=adam_epsilon)
        # hyper_parameters['adam_learning_rate'] = adam_learning_rate
        # hyper_parameters['adam_epsilon'] = adam_epsilon
        # print('\tadam_learning_rate', adam_learning_rate)
        # print('\tadam_epsilon', adam_epsilon)
        optimizer = keras.optimizers.Adam(learning_rate)
    elif (learning_rate_type == 'cyclical'
          or learning_rate_type == 'step'
          or learning_rate_type == 'sgd'):
        momentum = 0.9
        optimizer = keras.optimizers.SGD(learning_rate, momentum=momentum)
        hyper_parameters['SGD_momentum'] = momentum

    model.compile(loss='categorical_crossentropy',
                  optimizer=optimizer,
                  metrics='accuracy')

    return model, hyper_parameters

