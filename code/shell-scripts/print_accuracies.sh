#!/usr/bin/env bash
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

cd "$parent_path"
for f in $(find "../classification/saved/$1" | grep txt)
do
  echo -e "\n$f"
  cat "$f"
done
