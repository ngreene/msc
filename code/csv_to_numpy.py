#!usr/bin/env Python3

# Python standard libraries
import os
import re

# External libraries
import pandas as pd
import numpy as np

# def csv_to_numpy(f_path: str) -> Optional[np.ndarray]:
#     df = pd.read_csv(f_path)
# 
#     return df.to_numpy()

in_path = 'data/raw_csv/'
out_path = 'data/raw_npy/'

if not os.path.exists(out_path): os.mkdir(out_path)

for subdir in os.listdir(in_path):
    if re.match('^[0-9][0-9]$', subdir):
        participant_dir = in_path + subdir + '/'
        for f in os.listdir(participant_dir):
            if re.match('^[0-9][0-9]-[0-9][0-9]-[0-9][0-9].csv$', f):
                arr = pd.read_csv(participant_dir + f).to_numpy()
                savename = out_path + f.split('.')[0] + '.npy'
                np.save(savename, arr)
                print('Saved', savename)

