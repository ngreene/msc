import os
import re

import numpy as np
from file_io import csv_to_numpy
from visualisation import plot_time_and_freq_domain, save_plot_time_and_freq_domain
from visualisation import plot_freq_log_scale, save_plot_freq_log_scale
from filtering import notch_filter
from preprocessing import get_clips_with_events

T = 0.0005 # seconds

raw_csv_dir = 'data/raw_csv/'
filtered_dir = 'data/notch_filtered/'

if not os.path.exists(filtered_dir): os.makedirs(filtered_dir)

for subdir in os.listdir(raw_csv_dir):
    if re.match('^[0-9][0-9]$', subdir):
        for fname in os.listdir(raw_csv_dir + subdir):
            if re.match('^[0-9][0-9]-[0-9][0-9]-[0-9][0-9][.]csv$', fname):
                full_path = raw_csv_dir + subdir + '/' + fname
                out_name = fname.split('.')[0]
                arr = csv_to_numpy(full_path)[:, 1:-1]
                arr_filtered = np.load(filtered_dir + fname.split('.')[0] + '.npy')[:, :-1]

                # save_plot_time_and_freq_domain(arr, title=out_name)
                # save_plot_time_and_freq_domain(arr_filtered, title=out_name+' filtered')
                # plot_time_and_freq_domain(arr, title=out_name)
                # plot_time_and_freq_domain(arr_filtered, title=out_name+' filtered')

                save_plot_freq_log_scale(arr_filtered, title=out_name)
                # plot_freq_log_scale(arr_filtered, title=out_name)

# clips_with_events = get_clips_with_events(source_dir=filtered_dir)
# print('\n{0}   {1:<7} {2}'.format('Clip', 'Sample', 'Time'))
# for k, v in clips_with_events.items():
#     mean = int(np.mean(v))
#     print('{0:7<}  {1:<7d} {2:.2f}'.format(k, mean, mean/2000.0))
# clip_names = sorted(set(clip_names))
# for cname in clip_names: print(cname)
