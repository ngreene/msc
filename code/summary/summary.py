# Python standard libraries
import os
import re
from collections import Counter

# External libraries
import tensorflow as tf
import numpy as np

# My files
import file_io

np.set_printoptions(formatter={'float': '{: 0.4f}'.format})

summaries = []


class SummaryCl:
    def __init__(self, source_path):
        self.source_path = source_path
        self.summaries = self._get_summaries()
        self.accuracies = self._get_accuracies()

    def _get_summaries(self):
        for f in os.listdir(self.source_path):
            if re.match('^.*[.]pkl$', f):
                metadata = file_io.serial_read(self.source_path + f)
                metadata['name'] = '.'.join(f.split('.')[0:-1])
                metadata['testing_predict_one_hot'] = tf.keras.utils.to_categorical(
                    np.argmax(metadata['testing_predict'], axis=1)
                )
                summaries.append(metadata)
        return summaries

    def _get_accuracies(self):
        class Accuracy:
            def __init__(slf, name, train_acc, val_acc, test_acc):
                slf.name = name
                slf.train_acc = train_acc
                slf.val_acc = val_acc
                slf.test_acc = test_acc

            def __lt__(slf, other):
                return slf.test_acc < other.test_acc

        accuracies = []
        for metadata in self.summaries:
            training_acc = metadata['training_final_accuracy']
            val_acc = metadata['validation_final_accuracy']
            testing_acc = metadata['testing_final_accuracy']
            name = metadata['name']
            accuracies.append(Accuracy(name=name, train_acc=training_acc, val_acc=val_acc, test_acc=testing_acc))
        accuracies.sort(reverse=True)
        return accuracies

    def _get_class_codes(self):
        class_codes = []
        for meta_data in self.summaries:
            for file_name in meta_data['testing_data']:
                start_index = file_name.index('-') + 1
                class_code = file_name[start_index:]
                class_codes.append(class_code)
        code_dict = {k: v for k, v in sorted(Counter(class_codes).items(), key=lambda item: item[0])}
        return code_dict

    def print_overall_accuracies(self):
        mean_test_acc = 0.0
        count = 0
        for acc in self.accuracies:
            print()
            print(acc.name)
            print('  {0:10} {1:.4f}'.format('train_acc', acc.train_acc))
            # print('  {0:10} {1:.4f}'.format('val_acc', acc.val_acc))
            print('  {0:10} {1:.4f}'.format('test_acc', acc.test_acc))
            mean_test_acc += acc.test_acc
            count += 1
        mean_test_acc /= count
        print('\nMean test accuracy {0:.4f}\n'.format(mean_test_acc))

    def print_counts(self):
        class_dict = self._get_class_codes()
        class_codes = list(class_dict.keys())
        total_counts = list(class_dict.values())
        correct_counts = [0] * len(class_codes)
        for meta_data in self.summaries:
            for i, file_name in enumerate(meta_data['testing_data']):
                if np.array_equal(meta_data['testing_labels'][i], meta_data['testing_predict_one_hot'][i]):
                    start_index = file_name.index('-') + 1
                    code = file_name[start_index:]
                    correct_counts[list(class_codes).index(code)] += 1
        correct_acc = 0
        total_acc = 0
        prev_class = 0
        for i in range(len(class_codes)):
            curr_class = int(class_codes[i][1])
            if curr_class > prev_class:
                if curr_class > 1:
                    print('\tTotal   {0:2}/{1}'.format(correct_acc, total_acc))
                    correct_acc = 0
                    total_acc = 0
                if curr_class == 1: emotion = 'Neutral'
                elif curr_class == 2: emotion = 'Calm'
                elif curr_class == 3: emotion = 'Happy'
                elif curr_class == 4: emotion = 'Sadness'
                elif curr_class == 5: emotion = 'Anger'
                elif curr_class == 6: emotion = 'Fear'
                elif curr_class == 7: emotion = 'Disgust'
                print('\n{0}'.format(emotion))
                prev_class = curr_class

            print('\t{0}   {1:2}/{2}'.format(class_codes[i], correct_counts[i], total_counts[i]))
            correct_acc += correct_counts[i]
            total_acc += total_counts[i]
        print('\tTotal   {0:2}/{1}'.format(correct_acc, total_acc))

    def get_models(self):
        models = []
        for f in os.listdir(self.source_path):
            if re.match('^.*[.]h5$', f):
                models.append(tf.keras.models.load_model(self.source_path + f))
        return models

    def model_seed(self):
        return self.summaries[0]['hyper_parameters']['seed_value']

