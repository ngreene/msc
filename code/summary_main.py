# My packages
from summary import SummaryCl


# ******************************************************************************
# Start of parameters
# ******************************************************************************

model_name = 'cnn_03'
source_path = 'old-saved2/cross-val/' + model_name + '/'
summary = SummaryCl(source_path)
summary.print_overall_accuracies()
summary.print_counts()
print('\nmodel seed', summary.model_seed())
models = summary.get_models()

# ******************************************************************************
# End of parameters
# ******************************************************************************


print()
