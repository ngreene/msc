#!/data/ngreene/anaconda3/envs/py37/bin/python3

# Python standard libraries
import os
import datetime

# External libraries
import random
import sys

from tensorflow import keras

# My files
from data_generation.data_generator import Data_Generator
from classification.model_validation import Validator
from classification.model_save import save
import classification.cnn as cnn
import classification.transformer as transformer
import util
import compression
import scheduling

# os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # make tensorflow quiet

# CUDA drivers for GPU
os.environ['CUDA_DEVICE_ORDER'] = 'PCI_BUS_ID'
os.environ['CUDA_VISIBLE_DEVICES'] = '1'

# ******************************************************************************
# Set parameters here
# ******************************************************************************

#seed = 4292049469938427454
seed = None

model_type = 'cnn'  # convolutional neural network
#model_type = 'trf'  # transformer

training_type = 'k_fold'  # k fold cross validation
# training_type = 'train_val_test'  # train, validation, and test split

#data_compression = False
#data_compression_type = 'rand-proj'
#padding = None

# Training parameters
k = 10  # for cross validation
batch_size = 256
#minibatch_size = 1
epochs = 6000
#downsampling_step = 1
patience = 200
early_stopping_callback = keras.callbacks.EarlyStopping(
    monitor='val_loss',  # monitored value
    min_delta=0,  # acceptable difference in monitored value
    patience=patience # number of epochs between checks
)

# Regularisation
optn_batch_norm = True
optn_dropout = True
#optn_gaussian_noise = False
augmentation_multiple = 30
noise_stddev = 0.2

# Learning rate parameters
#learning_rate_type = 'adam'
learning_rate_type = 'sgd'
# learning_rate_type = 'cyclical'
# learning_rate_type = 'step'
max_learning_rate = 0.1
min_learning_rate = 0.01
step_size = 40


# ******************************************************************************
# End of parameters
# ******************************************************************************


# data_dir = '../data/uncompressed/{0}-pad/'.format(padding)
data_dir = '/data/ngreene/msc/code/data/notch_filtered/'
v = Validator(data_dir)
n_classes = v.n_classes

# Metadata for saving
class MetaData(object): pass
metadata = MetaData()
metadata.batches = batch_size
metadata.early_stopping_patience = patience
metadata.data_path = data_dir


def create_data_generators(train_cl, train_lb, val_cl, val_lb, test_cl, test_lb):
    training_generator = Data_Generator(
        list_IDs=train_cl,
        labels=train_lb,
        n_classes=n_classes,
        batch_size=batch_size,
        augmentation_multiple=augmentation_multiple,
        noise_stddev=noise_stddev
    )
    validation_generator = Data_Generator(
        list_IDs=val_cl,
        labels=val_lb,
        n_classes=n_classes,
        batch_size=batch_size
    )
    test_generator = Data_Generator(
        list_IDs=test_cl,
        labels=test_lb,
        n_classes=n_classes,
        batch_size=batch_size,
        shuffle=False
    )

    return training_generator, validation_generator, test_generator


def train_and_evaluate(training_generator, validation_generator, test_generator, random_seed):
    if model_type == 'cnn':
        model, hyper_parameters = cnn.build_model(
            training_generator.input_shape, n_classes,
            optn_batch_norm=optn_batch_norm,
            optn_dropout=optn_dropout,
            learning_rate_type=learning_rate_type,
            random_seed=random_seed
        )
    elif model_type == 'trf':
        model, hyper_parameters = transformer.build_model(
            input_shape=training_generator.input_shape,
            n_classes=n_classes,
            head_size=None,
            num_heads=None,
            ff_dim=None,
            num_transformer_blocks=None,
            mlp_units=None,
            mlp_dropout=0.4,
            dropout=0.25,
            learning_rate_type=learning_rate_type,
            random_seed=seed
        )

    log_dir = 'logs/fit/' + datetime.datetime.now().strftime('%Y%m%d-%H%M%S')
    callbacks = [early_stopping_callback,
                 keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)]

    training_generator.shuffle = False
    validation_generator.shuffle = False
    initial_train_loss, initial_train_acc = model.evaluate(training_generator)
    initial_val_loss, initial_val_acc = model.evaluate(validation_generator)
    initial_test_loss, initial_test_acc = model.evaluate(test_generator)
    training_generator.shuffle = True
    validation_generator.shuffle = True

    if learning_rate_type == 'cylical':
        callbacks += [scheduling.cyclical_decay_scheduler(
            step_size=step_size,
            min_learning_rate=min_learning_rate,
            max_learning_rate=max_learning_rate)]

    train_info = model.fit(
        training_generator,
        validation_data=validation_generator,
        epochs=epochs,
        callbacks=callbacks,
        workers=1
    )

    training_generator.shuffle = False
    validation_generator.shuffle = False

    final_train_loss, final_train_acc = model.evaluate(training_generator)
    final_val_loss, final_val_acc = model.evaluate(validation_generator)
    final_test_loss, final_test_acc = model.evaluate(test_generator)

    print('\nInitial train loss (tf): %.2f' % initial_train_loss)
    print('Initial val loss (tf): %.2f' % initial_val_loss)
    print('Initial test loss (tf): %.2f' % initial_test_loss)

    print('\nFinal train loss (tf): %.2f' % final_train_loss)
    print('Final val loss (tf): %.2f' % final_val_loss)
    print('Final test loss (tf): %.2f' % final_test_loss)

    print('\nInitial train accuracy (tf): %.2f' % initial_train_acc)
    print('Initial val accuracy (tf): %.2f' % initial_val_acc)
    print('Initial test accuracy (tf): %.2f\n' % initial_test_acc)

    print('\nFinal train accuracy (tf): %.2f' % final_train_acc)
    print('Final val accuracy (tf): %.2f' % final_val_acc)
    print('Final test accuracy (tf): %.2f' % final_test_acc)

    training_generator.shuffle = False
    validation_generator.shuffle = False

    metadata.training_data = [x.split('/')[-1].split('.')[0] for x in training_generator.list_IDs]
    metadata.validation_data = [x.split('/')[-1].split('.')[0] for x in validation_generator.list_IDs]
    metadata.testing_data = [x.split('/')[-1].split('.')[0] for x in test_generator.list_IDs]
    metadata.training_labels = training_generator.one_hot_encoded_labels()
    metadata.validation_labels = validation_generator.one_hot_encoded_labels()
    metadata.testing_labels = test_generator.one_hot_encoded_labels()
    metadata.training_predict = model.predict(training_generator)
    metadata.validation_predict = model.predict(validation_generator)
    metadata.testing_predict = model.predict(test_generator)
    metadata.training_loss = train_info.history['loss']
    metadata.training_accuracy = train_info.history['accuracy']
    metadata.validation_loss = train_info.history['val_loss']
    metadata.validation_accuracy = train_info.history['val_accuracy']
    metadata.training_initial_accuracy = initial_train_acc
    metadata.validation_initial_accuracy = initial_val_acc
    metadata.testing_initial_accuracy = initial_test_acc
    metadata.training_final_accuracy = final_train_acc
    metadata.validation_final_accuracy = final_val_acc
    metadata.testing_final_accuracy = final_test_acc
    metadata.training_initial_loss = initial_train_loss
    metadata.validation_initial_loss = initial_val_loss
    metadata.testing_initial_loss = initial_test_loss
    metadata.training_final_loss = final_train_loss
    metadata.validation_final_loss = final_val_loss
    metadata.testing_final_loss = final_test_loss
    metadata.hyper_parameters = hyper_parameters
    metadata.log_dir = log_dir
    metadata.augmentation_multiple = augmentation_multiple
    metadata.noise_stddev = noise_stddev

    return model, final_train_acc, final_test_acc


if training_type == 'k_fold':
    data_folds, label_folds = v.k_fold_stratified(k)  # Data and labels are partitioned into k folds
    if not seed:
        seed = random.randrange(sys.maxsize)
    # seed = 2939430397257476193
    for i in range(k):
        # Test data is the current fold
        test_data = data_folds[i]
        test_labels = label_folds[i]

        # Training data is all the remaining k-1 folds
        train_data = [cl for index, cl in enumerate(data_folds) if index != i]
        train_labels = [labels for index, labels in enumerate(label_folds) if index != i]
        # Flatten the lists
        train_data = [item for sublist in train_data for item in sublist]
        train_labels = [item for sublist in train_labels for item in sublist]

        # 10% of the training data is validation data
        train_data, train_labels, val_data, val_labels = util.stratified_split(train_data, train_labels, ratio=0.9)

        args = (train_data, train_labels, val_data, val_labels, test_data, test_labels)
        model, train_acc, test_acc = train_and_evaluate(*create_data_generators(*args), random_seed=seed)
        save(model, model_type=model_type, train_acc=train_acc, test_acc=test_acc, folds=k,
             metadata=metadata.__dict__)

elif training_type == 'train_val_test':
    args = v.train_validation_test_split()
    model, train_acc, test_acc = train_and_evaluate(*create_data_generators(*args))
    save(model, model_type=model_type, train_acc=train_acc, test_acc=test_acc, folds=k,
         metadata=metadata.__dict__)
