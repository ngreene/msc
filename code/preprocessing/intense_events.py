# Python standard libaries
import os
import re

# External libraries
import numpy as np

# My libraries
from file_io import serial_write, serial_read


def get_clips_with_events(source_dir, out_dir='data/events/'):
    if not os.path.exists(out_dir): os.makedirs(out_dir)
    out_fname = out_dir + 'clips_with_events.pkl'
    if os.path.exists(out_fname): return serial_read(out_fname)

    source_fnames = []
    for source_fname in os.listdir(source_dir):
        if re.match('^[0-9][0-9]-[0-9][0-9]-[0-9][0-9][.]npy$', source_fname):
            source_fnames.append(source_fname)

    clips_with_events = {}
    prev_participant = '01'
    for source_fname in sorted(source_fnames):
        arr = np.load(source_dir + source_fname)
        ii = np.where(arr[:, -1] > 0.5)[0]
        curr_participant = source_fname.split('-')[0]
        if ii.size > 0:
            if prev_participant != curr_participant:
                print()
                prev_participant = curr_participant
            print(source_fname[:-4], ii)
            cname = source_fname[3:-4]
            prev_dict_value = clips_with_events.get(cname, [])
            clips_with_events[cname] = prev_dict_value + [ii]

    clips_with_events = {k: np.sort(np.concatenate(v)) for k, v in
                         sorted(clips_with_events.items(), key=lambda item: item[0])}
    serial_write(clips_with_events, out_fname)

    return clips_with_events
