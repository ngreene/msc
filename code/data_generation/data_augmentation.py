import numpy as np


class augmentor:
    def __init__(self, seed=None):
        self.rng = np.random.default_rng(seed)

    def generate(self, x, m, std_dev=0.2):
        '''Generates augmented data from a batch of data
        x - The data to augment
        m - the augmentation multiple
        '''
        out_batches = [x]
        for _ in range(1, m):
            out_batches.append(x + self.rng.normal(0, std_dev, x.shape))
        return out_batches

