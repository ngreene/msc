# Python standard libraries
import math
import time

# External libraries
import numpy as np
import pandas as pd
import tensorflow as tf

# My files
from preprocessing import get_clips_with_events


class Data_Generator(tf.keras.utils.Sequence):

    def __init__(self, list_IDs, labels, n_classes, batch_size, n_channels=4,
                 shuffle=True, seed=55, augmentation_multiple=1, 
                 noise_stddev=0.2):
        self.batch_size = batch_size
        self.aug_mul = augmentation_multiple
        self.noise_stddev = noise_stddev
        self.list_IDs = list_IDs * self.aug_mul
        # Gaussian noise is added where noise_mask == 1 (for data augmentation)
        self.noise_mask = np.ones(len(self.list_IDs))
        self.noise_mask[:len(list_IDs)] = 0
        self.labels = labels * self.aug_mul
        self.n_classes = n_classes
        self.n_channels = n_channels
        min_size = np.load(list_IDs[0]).shape[0]
        for ID in list_IDs[1:]:
            size = np.load(ID).shape[0]
            if size < min_size: min_size = size
        self.input_shape = (None, min_size, self.n_channels)
        self.shuffle = shuffle
        self.rng = np.random.default_rng(seed)
        source_dir = '/'.join(list_IDs[0].split('/')[:-1]) + '/'
        self._clips_with_events = get_clips_with_events(source_dir)
        self.on_epoch_end()

    def __len__(self):
        'Returns the number of batches per epoch'
        return math.ceil(len(self.list_IDs) / float(self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        begin = index*self.batch_size
        end = (index+1)*self.batch_size

        if end > (len(self.indices)):
            end = len(self.indices)

        indices = self.indices[begin:end]

        list_IDs_temp = [self.list_IDs[i] for i in indices]
        labels_temp = [self.labels[i] for i in indices]
        noise_mask = self.noise_mask[indices]

        X, y = self.__data_generation(list_IDs_temp, labels_temp)

        # Add gaussian noise to unmasked indices
        noise = self.rng.normal(0, self.noise_stddev, X.shape)
        noise[np.where(noise_mask == 0)] = 0
        X += noise

        return X, y

    def on_epoch_end(self):
        'Update index after each epoch'
        self.indices = np.arange(len(self.list_IDs))
        if self.shuffle:
            self.rng.shuffle(self.indices)

    def __data_generation(self, list_IDs_temp, labels_temp):
        'Generates data containing batch_size samples'
        def random_middle_index(min, max, size):
            lowest_possible = min + size // 2
            highest_possible = max - (size + 1) // 2
            if lowest_possible >= highest_possible:
                middle_index = highest_possible
            else:
                middle_index = self.rng.integers(low=lowest_possible, 
                        high=highest_possible)
            return middle_index

        batch_size = len(list_IDs_temp)
        window_size = self.input_shape[1]
        X = np.zeros((batch_size, window_size, self.input_shape[2]))
        y = np.zeros(batch_size, dtype=int)

        for i, fname in enumerate(list_IDs_temp):
            arr = np.load(fname)[:,:-1]
            # ID = fname.split('.')[0][-5:]
            # events = self._clips_with_events.get(ID, None)
            # if events is not None:
            #     middle_index = int(np.median(events))
            #     # print('!', end='')
            # else:
            #     rows = arr.shape[0]
            #     middle_index = random_middle_index(int((2/3)*rows), rows, 
            #             window_size)
            # start_index = middle_index - window_size//2
            # if start_index < 0:
            #     start_index = 0
            # end_index = start_index + window_size
            # if end_index > arr.shape[0]:
            #     end_index = arr.shape[0]
            #     start_index = end_index - window_size

            rows = arr.shape[0]
            if rows > window_size:
                middle_index = random_middle_index(min=int((2.0/3.0)*rows),
                                                   max=rows,
                                                   size=window_size)
            else:
                middle_index = window_size // 2
            
            start_index = middle_index - window_size//2
            end_index = middle_index + (window_size+1)//2
            try:
                X[i] = arr[start_index:end_index]
            except ValueError as e:
                print()
                print('!', rows, start_index, end_index, end_index-start_index)
                print()
                raise e

            #X[i] = arr[-window_size:]
            y[i] = labels_temp[i]

        return X, tf.keras.utils.to_categorical(y, num_classes=self.n_classes)

    def one_hot_encoded_labels(self):
        y = np.array(self.labels, dtype=int)
        return tf.keras.utils.to_categorical(y, num_classes=self.n_classes)

