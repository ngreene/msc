import math

import tensorflow as tf
import tensorflow.keras as keras

global max_lr, min_lr, stepsize


def step_decay(epoch):
    base_lr = 0.1
    drop = 0.9
    epochs_drop = 10
    return base_lr * math.pow(drop, math.floor((1+epoch)/float(epochs_drop)))


def cyclical_decay(epoch):
    def triangle_wave(time, period, amplitude):
        return (4.0 * amplitude / period
                * abs(((time - period / 4.0) % period) - period / 2.0)
                - amplitude)
    global max_lr, min_lr, stepsize
    return triangle_wave(epoch, stepsize*2, (max_lr-min_lr)/2.0) + (max_lr+min_lr)/2.0


def step_decay_scheduler():
    return keras.callbacks.LearningRateScheduler(step_decay)


def cyclical_decay_scheduler(step_size, min_learning_rate, max_learning_rate):
    global max_lr, min_lr, stepsize
    max_lr = max_learning_rate
    min_lr = min_learning_rate
    stepsize = step_size
    return keras.callbacks.LearningRateScheduler(cyclical_decay)

class CustomSchedule(keras.optimizers.schedules.LearningRateSchedule):
    def __init__(self, d_model, warmup_steps=4000):
        super(CustomSchedule, self).__init__()

        self.d_model = d_model
        self.d_model = tf.cast(self.d_model, tf.float32)
        
        self.warmup_steps = warmup_steps
        
    def __call__(self, step):
        arg1 = tf.math.rsqrt(step)
        arg2 = step * (self.warmup_steps ** -1.5)

        return tf.math.rsqrt(self.d_model) * tf.math.minimum(arg1, arg2)
        
