import re
import os

import numpy as np
from sklearn import random_projection


def _compress(fnames: [str], R: np.ndarray) -> [np.ndarray]:
    ret = []
    for i, fname in enumerate(fnames):
        print('\r{0:>3d}/{1} files compressed'.format(i+1, len(fnames)), end='')
        ret.append(np.dot(R, np.load(fname)))
    print()
    return ret


def _save_data(data_lst: [np.ndarray], fnames: [str], out_dir: str):
    if not os.path.exists(out_dir): os.makedirs(out_dir)
    for data, fname in zip(data_lst, fnames):
        save_name = out_dir + fname.split('/')[-1]
        np.save(save_name, data)
    print('Compressed data saved to', out_dir)


def gaussian_random_projection(eps=0.1, pad='zero', seed=None):
    '''Compresses the data in the source directory with random projection and saves the resulting compressed data.

    :param eps: a positive value that determines the acceptable distortion in the reduction - higher values give greater
    reduction but greater distortion
    :param source_dir: the source directory path where the original (uncompressed) data is retrieved
    :param tag: an optional string tag to append to the name of the output directory path
    :param seed: the seed for random number generation (for reproducibility)
    :return: the output directory path where the resulting compressed data is stored
    '''
    out_dir = '../data/compressed/rand-proj-{0}-{1}/'.format(eps, pad)
    if os.path.exists(out_dir): return out_dir
    source_dir = '../data/uncompressed/' + pad + '-pad/'
    fnames = []
    for trial in os.listdir(source_dir):
        if re.match('^[0-9][0-9]-0[2-7]-0[1-3][.]npy', trial):
            fnames.append(source_dir + trial)

    print('\nGenerating random projection matrix...')
    grp_tr = random_projection.GaussianRandomProjection(eps=eps, random_state=seed)
    data_instance = np.load(fnames[0]).T
    grp_tr.fit(data_instance)
    print('Reducing {0} dimensions to {1}'.format(data_instance.shape[1], grp_tr.n_components_))
    reduced = []
    for i, fname in enumerate(fnames):
        print('\r{0:>3d}/{1} files compressed'.format(i+1, len(fnames)), end='')
        reduced.append(grp_tr.transform(np.load(fname).T).T)
    print()

    _save_data(reduced, fnames, out_dir)

    return out_dir
