import os
import random
import re
import pickle
import sys

import numpy as np
import cv2
import tensorflow as tf
from tensorflow.keras.regularizers import l2
import matplotlib.pyplot as plt
from tqdm.keras import TqdmCallback
from sklearn.metrics import classification_report, confusion_matrix

from classification.model_validation import Validator
import util
import file_io


os.environ['CUDA_DEVICE_ORDER'] = 'PCI_BUS_ID'
os.environ['CUDA_VISIBLE_DEVICES'] = '1'  # Assign GPU

data_path = 'data/InceptionResNetV2/'
data_fname = 'data.npy'
row_to_trial_fname = 'row_to_trial_dict.pkl'
num_classes = 7
if not os.path.exists(data_path):
    in_path = 'data/spec_img/'
    file_names = []
    labels = []
    row_to_trial = {}

    row = 0
    for subdir in os.listdir(in_path):
        if re.match('^(cor|ecg|res|zyg)$', subdir):
            full_path = in_path + subdir + '/'
            for f in os.listdir(full_path):
                if re.match('.*00[.]png$', f):
                    file_names.append(full_path + f)
                    trial_name = subdir + '/' + f.split('_')[0]
                    row_to_trial[row] = trial_name
                    row += 1

    N = len(file_names)
    X = np.zeros((N, 299, 299, 3))
    # y = tf.keras.utils.to_categorical(np.array(labels), num_classes=num_classes)
    for i, fname in enumerate(file_names):
        X[i] = cv2.imread(fname)

    os.makedirs(data_path)
    with open(data_path + row_to_trial_fname, 'wb') as f:
        pickle.dump(row_to_trial, f)
    np.save(data_path + data_fname, X)
    print()
else:
    preprocessed_fname = 'data_preprocessed.npy'
    if not os.path.exists(data_path + preprocessed_fname):
        data = np.load(data_path + data_fname)
        base_model = tf.keras.applications.InceptionResNetV2(include_top=False, weights='imagenet')
        inputs = tf.keras.Input(shape=(None, None, 3))
        x = base_model(inputs)
        outputs = tf.keras.layers.GlobalAvgPool2D()(x)
        model = tf.keras.models.Model(inputs, outputs)
        model_output = model.predict(data)
        np.save(data_path + preprocessed_fname, model_output)
    else:
        data = np.load(data_path + preprocessed_fname)
        with open(data_path + row_to_trial_fname, 'rb') as f:
            row_to_trial = pickle.load(f)
        # modality = 'cor' # Corrugator fEMG
        # modality = 'ecg' # ECG/Heartbeat
        # modality = 'res' # Respiration
        # modality = 'zyg' # Zygomaticus fEMG

        seeds = []
        for _ in range(100):
            seeds.append(random.randrange(2**32))
        # seeds = [3193505269] * k
        for seed in seeds:
            class MetaData(object):
                pass
            metadata = MetaData()

            tf.random.set_seed(seed)
            np.random.seed(seed)
            hidden_layers = []
            l_num = np.random.randint(3, 9)
            for _ in range(l_num):
                hidden_layers.append(np.random.choice([1024, 512, 258, 128]))
            hidden_layers = sorted(hidden_layers, reverse=True)
            l2_factor = np.random.uniform(0.0, 0.1)
            batch_size = np.random.choice([70, 140, 280])

            metadata.hidden_layers = hidden_layers
            metadata.l2_factor = l2_factor
            metadata.batch_size = batch_size

            modalities = ['cor', 'ecg', 'res', 'zyg']
            for modality in modalities:
                relevant_row_to_trial = {row: trial for row, trial in row_to_trial.items()
                                         if re.match('^{0}/.*'.format(modality), trial)}
                validator = Validator(data_dir=data_path, row_to_trial=relevant_row_to_trial)
                k = 10
                row_folds, label_folds = validator.k_fold_stratified(k)
                num_classes = validator.n_classes
                for i in range(k):
                    # Test data is the current fold
                    test_rows = row_folds[i]
                    test_labels = label_folds[i]

                    # Training data is all the remaining k-1 folds
                    train_rows = [cl for index, cl in enumerate(row_folds) if index != i]
                    train_labels = [labels for index, labels in enumerate(label_folds) if index != i]
                    # Flatten the lists
                    train_rows = [item for sublist in train_rows for item in sublist]
                    train_labels = [item for sublist in train_labels for item in sublist]

                    # 10% of the training data is validation data
                    # train_rows, train_labels, val_rows, val_labels = util.stratified_split(train_rows, train_labels, ratio=0.9)
                    X_train = data[train_rows]
                    # X_val = data[val_rows]
                    X_test = data[test_rows]
                    y_train = tf.keras.utils.to_categorical(np.array(train_labels), num_classes=num_classes)
                    # y_val = tf.keras.utils.to_categorical(np.array(val_labels), num_classes=num_classes)
                    y_test = tf.keras.utils.to_categorical(np.array(test_labels), num_classes=num_classes)


                    indices = np.arange(len(train_labels))

                    np.random.shuffle(indices)
                    X_train = X_train[indices]
                    y_train = y_train[indices]



                    inputs = tf.keras.Input(shape=(X_train.shape[-1]))
                    x = inputs
                    for units in hidden_layers:
                        x = tf.keras.layers.Dense(units, activation='relu',
                                                  kernel_regularizer=l2(l2_factor),
                                                  bias_regularizer=l2(l2_factor))(x)
                    outputs = tf.keras.layers.Dense(num_classes, activation='softmax')(x)
                    model = tf.keras.models.Model(inputs, outputs)
                    model.compile(
                        optimizer=tf.keras.optimizers.Adam(),
                        # optimizer=tf.keras.optimizers.SGD(momentum=0.9),
                        loss='categorical_crossentropy',
                        metrics='accuracy')
                    # early_stopping_callback = tf.keras.callbacks.EarlyStopping(
                    #     monitor='val_loss',  # monitored value
                    #     min_delta=0,  # acceptable difference in monitored value
                    #     patience=300# number of epochs between checks
                    # )
                    summary_dir = 'tmp/' + modality + '/' + str(seed) + '/fold_{0:02d}/'.format(i+1)
                    if not os.path.exists(summary_dir): os.makedirs(summary_dir)
                    checkpoint_filepath = summary_dir + 'weights-{epoch:04d}.hdf5'
                    # checkpoint_filepath = 'tmp/weights.hdf5'
                    # if not os.path.exists('tmp'):
                    #     os.makedirs('tmp')
                    model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
                        filepath=checkpoint_filepath,
                        save_weights_only=True,
                        save_best_only=True,
                        monitor='accuracy',#'val_accuracy',
                        mode='max'
                    )


                    print('\nFold {0:2d}/{1}'.format(i+1, k))
                    train_info = model.fit(x=X_train, y=y_train,
                                           batch_size=batch_size,
                                           # validation_data=(X_val, y_val),
                                           epochs=12000,
                                           verbose=0,
                                           callbacks=[
                                               # early_stopping_callback,
                                               model_checkpoint_callback,
                                               TqdmCallback(verbose=0)
                                           ])

                    metadata.training_loss_hist = train_info.history['loss']
                    metadata.training_accuracy_hist = train_info.history['accuracy']
                    # metadata.validation_loss_hist = train_info.history['val_loss']
                    # metadata.validation_accuracy_hist = train_info.history['val_accuracy']

                    cp_files = os.listdir(summary_dir)
                    checkpoints = [cp for cp in cp_files if re.match('.*hdf5', cp)]
                    best = sorted(checkpoints)[-1]
                    model.load_weights(summary_dir + best)
                    metadata.best_epoch = best.split('.')[0].split('-')[-1].lstrip('0')
                    print(metadata.best_epoch, best)
                    for c in checkpoints:
                        os.remove(summary_dir + c) # remove not needed files
                    final_train_loss, final_train_acc = model.evaluate(X_train, y_train)
                    # final_val_loss, final_val_acc = model.evaluate(X_val, y_val)
                    final_test_loss, final_test_acc = model.evaluate(X_test, y_test)
                    metadata.final_train_loss = final_train_loss
                    # metadata.final_val_loss = final_val_loss
                    metadata.final_test_loss = final_test_loss
                    metadata.final_train_accuracy = final_train_acc
                    # metadata.final_val_accuracy = final_val_acc
                    metadata.final_test_accuracy = final_test_acc

                    y_pred = np.argmax(model.predict(X_test), axis=1)
                    y_true = np.argmax(y_test, axis=1)
                    metadata.y_pred = y_pred
                    metadata.y_true = y_true

                    file_io.serial_write(metadata.__dict__, summary_dir + 'metadata.pkl')

                    # print(classification_report(y_true=y_true, y_pred=y_pred,
                    #                             target_names=[
                    #                                 'Calm', 'Happy', 'Sadness',
                    #                                 'Anger', 'Fear', 'Disgust'
                    #                             ]))
                    # confusion_mat = confusion_matrix(y_true=y_true, y_pred=y_pred)
                    # accuracies = confusion_mat.diagonal()/confusion_mat.sum(axis=1)





                # np.set_printoptions(precision=2)
                # train_losses = np.array(train_losses)
                # val_losses = np.array(val_losses)
                # test_losses = np.array(test_losses)
                # train_accuracies = np.array(train_accuracies)
                # val_accuracies = np.array(val_accuracies)
                # test_accuracies = np.array(test_accuracies)
                # print('\nSeeds', seeds)
                # print('\nLosses')
                # print('  Train:', train_losses)
                # print('  Val:  ', val_losses)
                # print('  Test: ', test_losses)
                # print('\nAccuracies')
                # print('  Train:', train_accuracies)
                # print('  Val:  ', val_accuracies)
                # print('  Test: ', test_accuracies, end='\n\n')
